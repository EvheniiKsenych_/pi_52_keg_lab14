﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB14
{
    class Elipse:Circle
    {
        private int x, x1, x2;
        private int y, y1, y2;
        private double ps1, ps2;
        private string color;
        public Elipse() { x = 0; x1 = 0; y = 0; y1 = 0;x2 = 0; y2 = 0; color = "black"; }
        public Elipse(int _x, int _y, int _x1, int _y1, int _x2, int _y2, string _color) { x = _x; x1 = _x1;x2 = _x2; y = _y; y1 = _y1;y2 = _y2; color = _color; }
        public Elipse(Elipse a) { x = a.x; x1 = a.x1;x2 = a.x2; y = a.y; y1 = a.y1;y2 = a.y2; color = a.color; }
        public override void Write()
        {
            Console.WriteLine("Elipse");
            Console.WriteLine("Color: " + color);
            Console.WriteLine("Point 1: x {0}, y {1}; Point 2: x {2}, y {3}; Point 3: x {4}, y {5};", x, y, x1, y1, x2,y2);
            Console.WriteLine("SQR: {0};", SQR());
            Console.WriteLine("PR: {0};", PR());
        }
        public override void ReSize()
        {
            Console.WriteLine("Elipse");
            Console.WriteLine("Enter x");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y");
            y = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter x1");
            x1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y1");
            y1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter x2");
            x2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y2");
            y2 = int.Parse(Console.ReadLine());
        }
        public override string Color
        {
            get { return color; }
            set { color = value; }
        }
        public void REsize(int _x, int _y, int _x1, int _y1, int _x2, int _y2)
        {
            x = _x; x1 = _x1; x2 = _x2; y = _y; y1 = _y1; y2 = _y2;
        }
        protected void GetPs()
        {
            ps1 = 2*  Math.Sqrt((x1 - x) * (x1 - x) + (y1 - y) * (y1 - y));
            ps2 = 2 * Math.Sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y));
            if(ps2>ps1)
            {
                double tmp = ps1;
                ps1 = ps2;
                ps2 = tmp;
            }
        }
        public override double SQR()
        {
            GetPs();
            return (3.14 * 2 * ps1 * 2 * ps2);
        }
        public override double PR()
        {
            return (4* (3.14*ps1*ps2+(ps1-ps2)/(ps1+ps2)));
        }
    }
}
