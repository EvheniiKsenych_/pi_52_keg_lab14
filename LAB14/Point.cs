﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB14
{
    abstract class Point
    {
       private int x, y;
       private string color;
       public Point() { x = 0;y = 0; color = "black"; }
       public Point(int _x, int _y, string _color) { x = _x; y = _y; color = _color; }
        public abstract string Color
        {
            set;
            get;
        }
        public abstract double SQR();
        public abstract double PR();
        public abstract void Write();
        public abstract void ReSize();
    }
}
