﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB14
{
    class Line:Point
    {
        private int x, y;
        private int x1, y1;
        private string color;
        public Line() { x = 0; x1 = 0; y = 0; y1 = 0; color = "black"; }
        public Line(int _x, int _x1,int _y, int _y1, string _color) { x = _x; x1 = _x1; y = _y; y1 = _y1; color = _color; }
        public Line(Line a) { x = a.x; x1 = a.x1; y = a.y; y1 = a.y1; color = a.color; }

        public override void Write()
        {
            Console.WriteLine("Line");
            Console.WriteLine("Color: " + color);
            Console.WriteLine("Point 1: x {0}, y {1}; Point 2: x {2}, y {3};",x,y,x1,y1);
            Console.WriteLine("SQR: {0};",SQR());
            Console.WriteLine("PR: {0};", PR());
        }
        public override void ReSize()
        {
            Console.WriteLine("Line");
            Console.WriteLine("Enter x");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y");
            y = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter x1");
            x1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y1");
            y1 = int.Parse(Console.ReadLine());
        }
        public void REsize(int _x, int _x1, int _y, int _y1)
        {
            x = _x; x1 = _x1; y = _y; y1 = _y1;
        }
        public override string Color
        {
            get{return color; }
            set { color = value;}
        }

        public override double SQR()
        {
            return 0;
        }
        public override double PR()
        {
            return Math.Sqrt((x1-x)* (x1 - x)+(y1-y)* (y1 - y));
        }
    }
}
