﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB14
{
    class Rectangle:Line
    {
        private int x, y;
        private int x1, y1;
        private string color;
        public Rectangle() { x = 0; x1 = 0; y = 0; y1 = 0; color = "black"; }
        public Rectangle(int _x, int _x1, int _y, int _y1, string _color) { x = _x; x1 = _x1; y = _y; y1 = _y1; color = _color; }
        public Rectangle(Rectangle a) { x = a.x; x1 = a.x1; y = a.y; y1 = a.y1; color = a.color; }

        public override void Write()
        {
            Console.WriteLine("Rectangle");
            Console.WriteLine("Color: " + color);
            Console.WriteLine("Point 1: x {0}, y {1}; Point 2: x {2}, y {3};", x, y, x1, y1);
            Console.WriteLine("SQR: {0};", SQR());
            Console.WriteLine("PR: {0};", PR());
        }
        public override void ReSize()
        {
            Console.WriteLine("Rectangle");
            Console.WriteLine("Enter x");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y");
            y = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter x1");
            x1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter y1");
            y1 = int.Parse(Console.ReadLine());
        }
        public override string Color
        {
            get { return color; }
            set { color = value; }
        }

        public override double SQR()
        {
            return (Math.Sqrt((y1 - y) * (y1 - y))* Math.Sqrt((x1 - x) * (x1 - x)));
        }
        public override double PR()
        {
            return (2*Math.Sqrt((y1 - y) * (y1 - y)) + 2*Math.Sqrt((x1 - x) * (x1 - x)));
        }
    }
}
